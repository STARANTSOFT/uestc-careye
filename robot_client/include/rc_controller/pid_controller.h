//
// Created by pulsarv on 2021/2/23.
//

#ifndef FOLLOW_DOWN_PID_CONTROLLER_H
#define FOLLOW_DOWN_PID_CONTROLLER_H
namespace follow_down {
    namespace move {
        class pid_controller {
        private:
            float kp;
            float ki;
            float kd;
            float target;
            float actual;
            float e;
            float e_pre_1;
            float e_pre_2;
            float A;
            float B;
            float C;

        public:
            pid_controller();

            /**
             * PID 初始化
             * @param p 比例参数
             * @param i 积分参数
             * @param d 微分参数
             */
            pid_controller(float p, float i, float d);

            /**
             * 执行PID步
             * @param tar target control val 目标数值
             * @param act actual output val  观测数值
             * @return
             */
            float pid_control(float tar, float act);

            void pid_show();
        };
    } // namespace move
} // namespace follow_down

#endif // FOLLOW_DOWN_PID_CONTROLLER_H
