//
// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCGYROTASK_H
#define UESTC_CAREYE_RCGYROTASK_H

#include <rc_system/data_struct.h>
#include <string>
#include "rc_task/rcBaseTask.h"
#include <libserv/libserv_message.h>
#include <libserv/libserv.h>

namespace rccore {
    namespace task {
        class GyroTask : public BaseTask, public libserv::Serial {
        public:
            explicit GyroTask(const std::shared_ptr<common::Context> &pcontext);

            void Run();

            void gyro_function(std::vector<char> message);

            ~GyroTask();

        private:
//            std::shared_ptr<libserv::Serial> pserial;
        };
    }
}


#endif //UESTC_CAREYE_RCGYROTASK_H
