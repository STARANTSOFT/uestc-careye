//
// Created by Pulsar on 2020/5/5.
//

#ifndef UESTC_CAREYE_RC_WEBSOCKET_H
#define UESTC_CAREYE_RC_WEBSOCKET_H

namespace rccore {
    namespace network {
        class Websocket {
        public:
            Websocket();

            ~Websocket();
        };
    }
}


#endif //UESTC_CAREYE_RC_WEBSOCKET_H
