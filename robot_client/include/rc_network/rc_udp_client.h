//
// Created by pulsarv on 19-5-2.
//

#ifndef UESTC_CAREYE_RC_UDP_CLIENT_H
#define UESTC_CAREYE_RC_UDP_CLIENT_H

#include <string>
namespace rccore {
    namespace network {
        class UdpClient {
            UdpClient();

            UdpClient(std::string address, int port);
        };
    }
}

#endif //UESTC_CAREYE_RC_UDP_CLIENT_H
