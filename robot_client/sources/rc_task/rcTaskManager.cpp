//
// Created by PulsarV on 18-5-14.
//
#include <rc_task/rcTaskManager.h>
#include <rc_task/rcMoveTask.h>
#include <rc_task/rcRadarTask.h>
#include <rc_task/rcNetworkTask.h>
#include <rc_task/rcGyroTask.h>
#include <rc_task/rcCVTask.h>
#include <rc_task/rcWebStream.h>

namespace rccore {
    namespace task {
        TaskManager::TaskManager(std::shared_ptr<common::Context> _pcontext) : pcontext(std::move(_pcontext)) {
        }

    }
}