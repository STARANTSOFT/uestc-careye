//
// Created by Pulsar on 2020/5/16.
//

#include <rc_task/rcNetworkTask.h>
#include <boost/asio.hpp>
#include <rc_task/rcWebStream.h>
#include <iostream>
#include <rc_network/rc_asny_tcp_client.h>
#include <rc_log/slog.hpp>

namespace rccore {
    namespace task {
        void run_network_task(std::shared_ptr<common::Context> pcontext) {
            slog::info << "正在连接网络...." << slog::endl;
            auto const address = boost::asio::ip::address_v4::from_string(
                    pcontext->pconfig->pconfigInfo->REMOTE_SERVER_IPADDRESS);
            boost::asio::ip::tcp::endpoint point(address, pcontext->pconfig->pconfigInfo->REMOTE_SERVER_PORT);
            network::AsnyTcpClient tcpClient(point);
            tcpClient.run();
        }

        void run_websocket_task(std::shared_ptr<common::Context> pcontext) {
            slog::info << "启动WEBSOCKET服务...." << slog::endl;

        }

        void NetworkTask::Run() {

        }

        NetworkTask::NetworkTask(std::shared_ptr<common::Context> pcontext) : BaseTask(std::move(pcontext)) {

        }

        NetworkTask::~NetworkTask() {

        }
    }
}
