# !/bin/sh
echo 3 > /sys/class/pwm/pwmchip0/export
echo 3413333 > /sys/class/pwm/pwmchip0/pwm3/period
echo 1706667 > /sys/class/pwm/pwmchip0/pwm3/duty_cycle
echo 1 > /sys/class/pwm/pwmchip0/pwm3/enable
echo 0 > /sys/class/pwm/pwmchip0/pwm3/enable

echo 1 > /sys/class/pwm/pwmchip0/export
echo 3413333 > /sys/class/pwm/pwmchip0/pwm1/period
echo 1706667 > /sys/class/pwm/pwmchip0/pwm1/duty_cycle
echo 1 > /sys/class/pwm/pwmchip0/pwm1/enable
echo 0 > /sys/class/pwm/pwmchip0/pwm1/enable

echo 0 > /sys/class/pwm/pwmchip0/export
echo 3413333 > /sys/class/pwm/pwmchip0/pwm0/period
echo 1706667 > /sys/class/pwm/pwmchip0/pwm0/duty_cycle
echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable
echo 0 > /sys/class/pwm/pwmchip0/pwm0/enable

echo 402 > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio402/direction
echo 1 > /sys/class/gpio/gpio402/value
echo 402 > /sys/class/gpio/unexport

echo 405 > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio405/direction
echo 1 > /sys/class/gpio/gpio405/value
echo 405 > /sys/class/gpio/unexport

echo 431 > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio431/direction
echo 1 > /sys/class/gpio/gpio431/value
echo 431 > /sys/class/gpio/unexport